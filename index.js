// ODM
/*
	-Object Document Mapper tool that translates object
*/

// Mongoose
/*
	-An ODM library that manage data relationships, validates schemas, and simplifies MongoDB document manipulation via the use of models.

*/

// Schemas
/*
	-Is a representation of a documents structure. it also contains a documents expected properties and data types.
*/

// Models

/*
	-A programming interface for querying or manipulating a database. a mongoose model contains methods that simplify such operations.
*/


const express = require("express");

// Mongoose is a package that allows creation of schemas to model our data structures
// it has access to a number of methods for manipulating our database
const mongoose = require ("mongoose");

const app = express();

const port = 3001;


// MongoDB connection

// Connect the database by passing in our connection string, remember to replace the password and database names with actual values.
// {newUrlParser:true} allows us to avoid any current and future errors while connecting to MongoDB


/*Syntax:
	mongoose.connect("<MongoDB connection string>", {userNewUrlParser:true})
*/

// Connecting to MongoDB atlas

mongoose.connect("mongodb+srv://admin:admin123@project0.mwq6bdq.mongodb.net/s35?retryWrites=true&w=majority",
	{
	useNewUrlParser:true,
	useUnifiedTopology: true

	}
);

// Set notifications for connection success/failure
// Connection to database
// allows us to handle errors when the initial connection is establish
// works with the "on" and "once" mongoose methods
let db = mongoose.connection;

db.on("error",console.error.bind(console,"connection error"));
// if a connection error occured, output in the console
// console.error.bind(console) allows us to print errors in our terminal


db.once("open",() => console.log("We're connected to the cloud database"))


// Mongoose schemas
/*
	-determine the structure of the documents to be written in the databse
	-schemas acts as blueprints to our data
	-use the schema constructor of the mongoose module to create a new schema object
	-the "new" keyword creates a brandnew schema
*/

const taskSchema = new mongoose.Schema({

// Define the fields with their correspanding data type
	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

// Models
/*
	-uses schemas and used to create for instantiate objects that correspond the schema
	-Server>Schema(blueprint)> Database>Collection
	-Singular form and capitalize
*/
const Task = mongoose.model("Task", taskSchema)

// setup for allowing the server to handle data from requests
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// Create a new Task
/*
	-Business Logic
		1. Add a functionality to check if there are duplicate tasks
			-if the task already exists in the database, we return an error
			-if the task does not exist in the database, we add it in the database
		2. The task data will be coming from the request's body
		3. Create a new Task object with a "name" field/property
		4. The "status" property does not need to be provided because our schemaa defaults it to "pending" upon creation of an object
*/

app.post("/tasks", (req, res)=>{

	Task.findOne({name:req.body.name},(err,result)=>{
		if (result != null && result.name == req.body.name) {
			return res.send("Duplicate task found");
		}else {
			let newTask = new Task({
				name: req.body.name
			});
			newTask.save((saveErr,savedTask)=> {
				if(saveErr) {
					return console.error(saveErr);
				}else{
					return res.status(201).send("New task created");
				}
			})
		}
	})
})

// Getting all the tasks
// Business logic
/*
	-retrieve all the documents
	-If an error is encountered, print the error
	-if no errors are found, send a success status back to the client/Postman and return an array of documents
*/

app.get("/tasks",(req, res) => {
	Task.find({}, (err,result)=>{
		if (err) {
			return console.log(err);
		}else {
			return res.status(200).json({
				data : result
			})
		}
	})
})





// Activity

// Schema
const userSchema = new mongoose.Schema({

	username: String,
	password: String
})

// Model
const User = mongoose.model("User", userSchema)

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.post("/signup", (req, res)=>{

	User.findOne({username:req.body.username},(err,result)=>{
		if (result != null && result.username === req.body.username) {
			return res.send("Duplicate Username found");
		}else {(req.body.username !== '' && req.body.password !== '')
			
				let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});
			newUser.save((saveErr,savedUser)=> {
				if(saveErr) {
					return console.error(saveErr);
				}else{
					return res.status(201).send("New user created");
				}
			})
			
		}
	})
})


app.get("/signup",(req, res) => {
	User.find({}, (err,result)=>{
		if (err) {
			return console.log(err);
		}else {
			return res.status(200).json({
				data : result
			})
		}
	})
})





app.listen(port,() => console.log(`Server running at port ${port}`));
